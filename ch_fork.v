Require Import ZArith.
Require Import ZArith_dec.
Require Import List.
Open Scope Z_scope.
Open Scope list_scope.

Module ListNotations.
Notation "[ ]" := nil (format "[ ]") : list_scope.
Notation "[ x ]" := (cons x nil) : list_scope.
Notation " x :: xs" := (cons x xs) : list_scope.
Notation "[ x ; y ; .. ; z ]" := (cons x (cons y .. (cons z nil) ..)) : list_scope.
Notation "[ x ; .. ; y ]" := (cons x .. (cons y nil) ..) (compat "8.4") : list_scope.
End ListNotations.

Import ListNotations.

(*** Z ***)
Definition Punto: Type := (Z * Z).

Definition neq_punto (a b : Punto) :=
  orb (Zneq_bool (fst a) (fst b))
      (Zneq_bool (snd a) (snd b)).

Compute neq_punto (2, 1) (2, 1).
Compute neq_punto (1, 2) (3, 2).

Theorem neq_eq: forall (a: Punto), neq_punto a a = false.
Proof.
intros.
unfold neq_punto.
unfold Zneq_bool.
destruct a.
simpl.
rewrite Zcompare_refl.
rewrite Zcompare_refl.
simpl.
trivial.
Qed.

Definition Puntos := list Punto.

(** Área signada... álgebra... determina de qué lado se encuentra c respecto a la recta ab **)
Definition vuelta_izq_b (a b c: Punto): bool := 
  Z.geb ((fst a - fst c) * (snd b - snd c) 
- (snd a - snd c) * (fst b - fst c)) 0.

Compute vuelta_izq_b (2, 1) (3, 3) (2, 3). (* true *)
Compute vuelta_izq_b (2, 1) (3, 3) (4, 5). (* colineal *)
Compute vuelta_izq_b (2, 1) (3, 3) (3, 0). (* false *)

Theorem colineal_eq: forall (a: Punto), vuelta_izq_b a a a = true.
Proof.
intros.
unfold vuelta_izq_b.
destruct a.
simpl.
assert (z - z = 0).
apply Zplus_opp_r.
rewrite H.
rewrite Zmult_0_l.
rewrite Zmult_0_r.
simpl.
constructor.
Qed.

Definition Nube := list Punto.

Fixpoint todos_un_lado (a b: Punto) (n: Nube): bool :=
  match n with
  | x::l => andb (vuelta_izq_b a b x) (todos_un_lado a b l)
  | _ => true
  end.

Definition arista_cierre (a b: Punto) (n: Nube): bool := andb (neq_punto a b) (todos_un_lado a b n).

Compute arista_cierre (2, 1) (3, 3) [(2, 1); (3, 3); (1, 3); (2, 2)]. (* true *)
Compute arista_cierre (2, 1) (3, 3) [(2, 1); (3, 3); (1, 3); (2, 2); (4, 0)]. (* false *)
Compute arista_cierre (2, 1) (2, 1) [(2, 1); (3, 3); (1, 3); (2, 2)]. (* false *)

Theorem no_puntos: forall (a: Punto) (n: Nube), arista_cierre a a n = false.
Proof.
intros.
unfold arista_cierre.
rewrite neq_eq.
simpl.
trivial.
Qed.

Definition Arista: Type := (Punto * Punto).
Definition Poligono := list Arista.

(** explicación de CH **)

Fixpoint son_de_cierre (aristas: Poligono) (n: Nube) : bool :=
match aristas with
| (a, b):: l => andb (arista_cierre a b n) (son_de_cierre l n)
| nil => true
end.

Fixpoint tiene_siguiente (a: Arista) (aristas: Poligono) : bool :=
match aristas with
| (p, q)::l => if neq_punto p (snd a) then tiene_siguiente a l else true
| nil => false
end.

(* true *)
Compute tiene_siguiente ((1, 2), (2, 1)) [((3, 2), (2, 1)); ((2, 1), (3, 3))].
(* false *)
Compute tiene_siguiente ((3, 3), (2, 1)) [((3, 2), (2, 1)); ((2, 2), (3, 3))].

Fixpoint cerrado (aristas: Poligono) (faltan: Poligono) : bool :=
match faltan with
| (a, b)::l => andb (tiene_siguiente (a, b) aristas) (cerrado aristas l)
| nil => true
end.

(* false *)
Compute cerrado [((2, 1), (1, 3))] [((2, 1), (1, 3))].
(* true *)
Compute cerrado [((2, 1), (1, 3)); ((1, 3), (2, 1))] [((2, 1), (1, 3)); ((1, 3), (2, 1))].

(* Theorem cerrado_siguiente: forall (p: Poligono), cerrado p p = true <-> forall (a: Arista), In a p -> tiene_siguiente a p = true.
Proof.
split.
intros.
induction p.
contradict H0.
destruct a0.
simpl in H.
 *)

Definition cierre_convexo (p: Poligono) (n: Nube): bool := andb (son_de_cierre p n) (cerrado p p).

(* true *)
Compute cierre_convexo [((7, 1), (11, 3)); ((11, 3), (11, 6)); ((11, 6), (7, 9)); ((7, 9), (3, 6)); ((3, 6), (3, 3)); ((3, 3), (7, 1))]  [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3); (6, 4); (8, 7); (8, 2)].
(* false *)
Compute cierre_convexo [((7, 1), (11, 3)); ((11, 3), (11, 6)); ((11, 6), (7, 9)); ((7, 9), (3, 6)); ((3, 6), (3, 3))]  [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3); (6, 4); (8, 7); (8, 2)].

(* Versión usando puntos del cierre

Fixpoint cierre_aux (p: Poligono) (n: Nube) : bool :=
  match p with
  | cons x l => match l with
     | cons y l' => andb (arista_cierre x y n) (cierre_aux l n)
     | nil => true
    end
  | nil => true
  end.

(* true *)
Compute cierre_aux [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3)]  [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3); (6, 4); (8, 7); (8, 2)].
(* true pero está mal, por eso es aux *)
Compute cierre_aux [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6)]  [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3); (6, 4); (8, 7); (8, 2)].

Definition cierre_convexo (p: Poligono) (n: Nube): bool := match p with
  | cons x l => cierre_aux (p ++ [x]) n
  | nil => false
  end.

(* sigue siendo true *)
Compute cierre_convexo [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3)]  [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3); (6, 4); (8, 7); (8, 2)].
(* ya no es true *)
Compute cierre_convexo [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6)]  [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3); (6, 4); (8, 7); (8, 2)].

Fixpoint mero_abajo_aux (n: Nube) (m: Punto): Punto :=
match n with
| cons x l => if Z.ltb (snd x) (snd m) then mero_abajo_aux l x else mero_abajo_aux l m
| nil => m
end.

Definition mero_abajo (n: Nube): option Punto :=
match n with
| cons x l => Some(mero_abajo_aux n x)
| nil => error
end.

Compute mero_abajo [].
Compute mero_abajo [(2, -1); (3, 10)].
Compute mero_abajo [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3)]. (* 7, 1 *)

Fixpoint no_está (p: Punto) (l: Poligono): bool :=
match l with
| nil => true
| cons x l' => if neq_punto x p then no_está p l' else false
end.

Compute no_está (2, 1) [(1, 2)]. (* true.. no está *)
Compute no_está (2, 2) [(1, 2); (2, 2)]. (* falso.. no es cierto que no está *)

Fixpoint busca_arista (p: Punto) (n: Nube) (faltan: Nube) (ya_estan: Poligono) : option Punto :=
  match faltan with
  | cons x l => if (andb (arista_cierre p x n) (no_está x ya_estan)) then Some x else busca_arista p n l ya_estan
  | nil => None
  end.

Compute busca_arista (7, 1) [(11, 6); (7, 9); (3, 6); (3, 3); (11, 3); (6, 4); (8, 7); (8, 2)] [(11, 6); (7, 9); (3, 6); (3, 3); (11, 3); (6, 4); (8, 7); (8, 2)] [(7, 1)].
Compute busca_arista (9, 5) [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3); (6, 4); (8, 7); (8, 2)] [(11, 6); (7, 9); (3, 6); (3, 3); (11, 3); (6, 4); (8, 7); (8, 2)] [].

Fixpoint jarvis_aux (n: Nube) (p: Punto) (l: Poligono): Poligono :=
  match busca_arista p n n l with
  | Some q => jarvis_aux n q (l ++ [q])
  | None => l
  end.

Fixpoint jarvis (n: Nube) : Poligono :=
  match mero_abajo n with
  | Some p => jarvis_aux n p [p]
  | None => []
  end.
*)

Fixpoint busca_arista (p: Punto) (n: Nube) (faltan: Nube) : option Punto :=
  match faltan with
  | x::l => if (arista_cierre p x n) then Some x else busca_arista p n l
  | nil => None
  end.

(* 11, 3 *)
Compute busca_arista (7, 1) [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3); (6, 4); (8, 7); (8, 2)] [(11, 6); (7, 9); (3, 6); (3, 3); (11, 3); (6, 4); (8, 7); (8, 2)].
(* Nel, ni es punto del cierre *)
Compute busca_arista (6, 4) [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3); (6, 4); (8, 7); (8, 2)] [(11, 6); (7, 9); (3, 6); (3, 3); (11, 3); (6, 4); (8, 7); (8, 2)].

Fixpoint naive_aux (n: Nube) (faltan: Nube) (si_están: Poligono): Poligono :=
  match faltan with
  | x::l => match busca_arista x n n with
    | Some q => naive_aux n l ((x, q) :: si_están)
    | None => naive_aux n l si_están
    end
  | nil => si_están
  end. (** Aquí es donde las definiciones dadas de vuelta_izq y arista_cierre toman sentido **)

Definition naive (n: Nube) : Poligono := naive_aux n n [].

(* [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3)] *)
Compute let n := [(7, 1); (11, 3); (11, 6); (7, 9); (3, 6); (3, 3); (6, 4); (8, 7); (8, 2)] in cierre_convexo (naive n) n.

Theorem naive_funciona: forall (n: Nube), cierre_convexo (naive n) n = true.
Proof.
intros.
induction n.
unfold naive.
simpl.
unfold cierre_convexo.
simpl.
trivial.
unfold naive.
simpl.
rewrite no_puntos.
destruct (busca_arista a (a :: n) n).
unfold cierre_convexo.
unfold cierre_convexo in IHn.
unfold naive in IHn.
admit.
unfold cierre_convexo.
admit.
(* no me molestes *)
(* no i don't *)
(* i really don't *)
(* whyyyyyyyyyy *)
